# ansible-role-s3sync

This Ansible role allows you to schedule jobs to sync data to S3.

## Requirements

In order to use this role you need a S3 bucket and AWS credentials with
read/write access to that bucket

## Role Variables

Mandatory variables are:

- `s3sync_aws_bucket:`: Name of the S3 bucket to sync to
- `s3sync_aws_access_key_id`: AWS Acces Key ID
- `s3sync_aws_secret_access_key`: AWS Secret Access Key

Check `defaults/main.yml` for default values for variables you can overwrite.

Optional examples are:
- `s3sync_systemd_user`: user to run the s3sync under. This user must have read rights on the source to sync. Default: s3sync
- `s3sync_schedule`[default:19:00]: determines when s3sync will be run. Any valid systemd timer
  value is allowed. Use `systemd-analyze calendar "<schedule>"` to test.
- `s3sync_paths:`: list of paths to sync. Defaults to `/data`
```yaml
s3sync_paths:
  - /data
  - /var/log
```
** Warning: symlinks are followed and synced separately. Make sure you you don't end up with double synced data folders for example.

## Dependencies

This role has no external dependencies.

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

```yaml
- name: Install S3sync
  hosts: all
  tags: s3sync
  gather_facts: false
  tasks:
    - name: Configure S3sync
      include_role:
        name: s3sync
        apply:
          tags: s3sync
```

## License

Apache-2.0

## Author Information

Naturalis Biodiversity Center
